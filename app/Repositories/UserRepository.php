<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version October 6, 2019, 5:38 am UTC
*/

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'username',
        'password',
        'device_type',
        'firstname',
        'lastname',
        'userid',
        'tax_registration_number',
        'address',
        'postcode',
        'city',
        'county',
        'state',
        'country',
        'phone',
        'mobile',
        'fax',
        'email',
        'email_verified_at',
        'verified',
        'remember_token'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
