<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RegisterUser extends Notification
{
    use Queueable;
    
    public $user;
    public $generatedpassword;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$generatedpassword)
    {
        $this->user=$user;
        $this->generatedpassword=$generatedpassword;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $encrypted = \Illuminate\Support\Facades\Crypt::encryptString($this->user->email);
        $generatedpassword=$this->generatedpassword;
        return (new MailMessage)
                    ->line('Your account has been successfully created in our system.Please click on below link to activate your account.')
                    ->line("Your one time password after verification is {$generatedpassword}")
                    ->action('Verify', url('/verify/'.$encrypted))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
