<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * @package App\Models
 * @version October 6, 2019, 5:38 am UTC
 *
 * @property string username
 * @property string password
 * @property string device_type
 * @property string firstname
 * @property string lastname
 * @property string userid
 * @property string tax_registration_number
 * @property string address
 * @property string postcode
 * @property string city
 * @property string county
 * @property string state
 * @property string country
 * @property string phone
 * @property string mobile
 * @property string fax
 * @property string email
 * @property string|\Carbon\Carbon email_verified_at
 * @property integer verified
 * @property string remember_token
 */
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;

    protected $guard_name = 'web';

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'username',
        'password',
        'device_type',
        'firstname',
        'lastname',
        'userid',
        'tax_registration_number',
        'address',
        'postcode',
        'city',
        'county',
        'state',
        'country',
        'phone',
        'mobile',
        'fax',
        'email',
        'email_verified_at',
        'verified',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'username' => 'string',
        'password' => 'string',
        'device_type' => 'string',
        'firstname' => 'string',
        'lastname' => 'string',
        'userid' => 'string',
        'tax_registration_number' => 'string',
        'address' => 'string',
        'postcode' => 'string',
        'city' => 'string',
        'county' => 'string',
        'state' => 'string',
        'country' => 'string',
        'phone' => 'string',
        'mobile' => 'string',
        'fax' => 'string',
        'email' => 'string',
        'email_verified_at' => 'datetime',
        'verified' => 'integer',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'username' => 'required',
//        'password' => 'required',
        'firstname' => 'required',
        'lastname' => 'required',
        'email' => 'required'
    ];

    
}
