<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Alert;
use Response;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Contracts\Encryption\DecryptException;

class UserController extends AppBaseController {

    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo) {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request) {
        $users = $this->userRepository->all();

        return view('users.index')
                        ->with('users', $users);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create() {
        $roles_data = Role::get();
        $roles = [];
        foreach ($roles_data as $role) {
            $roles[$role->name] = $role->name;
        }
        return view('users.create')->with('roles', $roles);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request) {
        $input = $request->all();
        $generatedpassword = \Illuminate\Support\Str::random(8);
        ;
        $input['password'] = Hash::make($generatedpassword);
        $input['username'] = $input['email'];

        $user = $this->userRepository->create($input);
        $user->assignRole($input['role']);
//        dispatch(new \App\Jobs\RegisterUser($user));// send email to user regarding registration
        $user->notify(new \App\Notifications\RegisterUser($user, $generatedpassword));
        Alert::success('User saved successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id) {
        $user = $this->userRepository->find($id);
//        $user= \Illuminate\Support\Facades\Auth::user();
//        dd($id);        
//        dd($user->getAllPermissions());
//        dd($user->hasPermissionTo('users'));

        if (empty($user)) {
            Alert::error('User not found');

            return redirect(route('users.index'));
        }
        $user_role = $user->getRoleNames();
        foreach ($user_role as $role) {
            $user->role = $role;
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        $user = $this->userRepository->find($id);
        if (empty($user)) {
            Alert::error('User not found');

            return redirect(route('users.index'));
        }
        $user_role = $user->getRoleNames();
        foreach ($user_role as $role) {
            $user->role = $role;
        }
        $roles_data = Role::get();
        $roles = [];
        foreach ($roles_data as $role) {
            $roles[$role->name] = $role->name;
        }

        return view('users.edit')->with('user', $user)->with('roles', $roles);
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request) {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Alert::error('User not found');

            return redirect(route('users.index'));
        }
        $input = $request->all();
//        $input['password']=Hash::make($input['password']);


        $user = $this->userRepository->update($input, $id);
        $user->syncRoles([$input['role']]);

        Alert::success('User updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id) {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Alert::error('User not found');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Alert::success('User deleted successfully.');

        return redirect(route('users.index'));
    }

    public function verifyUser($email) {
        $user = null;
        try {
            $decrypted = \Illuminate\Support\Facades\Crypt::decryptString($email);
            $user = \App\Models\User::where('email', $decrypted)->where('deleted_at', null)->first();
        } catch (DecryptException $e) {
            Alert::error('User not found');
        }
        if (!$user) {
            Alert::error('User not found');
            return view('auth.login');
        }
        User::where('email', $decrypted)->update(['verified' => 1]);
        Alert::success('User verified successfully.');
        return view('auth.login');
    }

    public function changePassword() {
        return view('auth.change_password');
    }

    public function changePasswordAction(Request $request) {
        $input = $request->all();
        $oldPassword = isset($input['oldPassword']) ? $input['oldPassword'] : "";
//        $hashedOldPassword=Hash::make($oldPassword);
        $user = \Illuminate\Support\Facades\Auth::user();
        if ($user) {
            $check = Hash::check($oldPassword, $user->password);
            if (!$check) {
                Alert::error('Old password was incorrect.');
                return redirect(route('changePassword'));
            }
            $newPassword = isset($input['newPassword']) ? $input['newPassword'] : "";
            $reenterPassword = isset($input['reenterPassword']) ? $input['reenterPassword'] : "";
            if ($newPassword == $reenterPassword) {
                $user = $this->userRepository->update(["password" => Hash::make($newPassword)], $user->id);
                Alert::success('Password changed successfully.');
            } else {
                Alert::error('Passwords did not match.');
            }
        }

//        return view('auth.change_password');
        return redirect(route('changePassword'));
    }

}
