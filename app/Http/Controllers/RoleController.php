<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Repositories\RoleRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Alert;
use Response;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleController extends AppBaseController
{
    /** @var  RoleRepository */
    private $roleRepository;

    public function __construct(RoleRepository $roleRepo)
    {
        $this->roleRepository = $roleRepo;
    }

    /**
     * Display a listing of the Role.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
//        $roles = $this->roleRepository->all();
        $roles= Role::get();        

        return view('roles.index')
            ->with('roles', $roles);
    }

    /**
     * Show the form for creating a new Role.
     *
     * @return Response
     */
    public function create()
    {
        $permissions= Permission::get();
        foreach ($permissions as $permission) {
            $permission->assigned=false;
        }        
        return view('roles.create')->with('permissions',$permissions);
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param CreateRoleRequest $request
     *
     * @return Response
     */
    public function store(CreateRoleRequest $request)
    {
        $input = $request->all();
        
        $role=Role::create(['name' => $input['name']]);
        if(isset($input['permissions'])){
            foreach ($input['permissions'] as $permission) {
                $permission= Permission::findById($permission);
                $role->givePermissionTo($permission);
            }
        }
//        $role = $this->roleRepository->create($input);

        Alert::success('Role saved successfully.');

        return redirect(route('roles.index'));
    }

    /**
     * Display the specified Role.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $permissions=$role->permissions;
        
        if (empty($role)) {
            Alert::error('Role not found');

            return redirect(route('roles.index'));
        }

        return view('roles.show')->with('role', $role)->with('permissions', $permissions);
    }

    /**
     * Show the form for editing the specified Role.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
//        $role = $this->roleRepository->find($id);
        $role = Role::find($id);
        $permissions= Permission::get();
        $granted_permissions=$role->permissions;
        $assigned=[];
        foreach ($permissions as $permission) {                 
            foreach ($granted_permissions as $permission2) {
                if($permission->id==$permission2->id){
                    $permission->assigned=true;
                }
            } 
        }
        if (empty($role)) {
            Alert::error('Role not found');

            return redirect(route('roles.index'));
        }

        return view('roles.edit')->with('role', $role)->with('permissions',$permissions);
    }

    /**
     * Update the specified Role in storage.
     *
     * @param int $id
     * @param UpdateRoleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoleRequest $request)
    {
        $input = $request->all();
//        $role = $this->roleRepository->find($id);
        $role = Role::find($id);

        if (empty($role)) {
            Alert::error('Role not found');

            return redirect(route('roles.index'));
        }
        
        
        $permissions= Permission::get();
        foreach ($permissions as $permission) {
            $role->revokePermissionTo($permission);
        }
        
        
        if(isset($input['permissions'])){
            foreach ($input['permissions'] as $permission) {
                $permission= Permission::findById($permission);
                $role->givePermissionTo($permission);
            }
        }
        $role = $this->roleRepository->update($request->all(), $id);
        

        Alert::success('Role updated successfully.');

        return redirect(route('roles.index'));
    }

    /**
     * Remove the specified Role from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $role = $this->roleRepository->find($id);

        if (empty($role)) {
            Alert::error('Role not found');

            return redirect(route('roles.index'));
        }

        $this->roleRepository->delete($id);

        Alert::success('Role deleted successfully.');

        return redirect(route('roles.index'));
    }
}
