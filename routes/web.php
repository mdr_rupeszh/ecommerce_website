<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'HomeController@index');
Route::get('/changePassword', 'UserController@changePassword')->name('changePassword');
Route::post('/changePassword', 'UserController@changePasswordAction')->name('changePasswordAction');
Auth::routes();


Route::get('/home', 'HomeController@index');

Route::get('/verify/{email}', 'UserController@verifyUser');
Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::resource('plans', 'PlanController');
