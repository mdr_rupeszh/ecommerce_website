	<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->


 
<head>
	<title>JellyNet - Bootstrap 4 HTML template</title>
	<meta charset="utf-8">
	<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animations.css">
	<link rel="stylesheet" href="css/regular.min.css">
	<link rel="stylesheet" href="css/brands.min.css">
	<link rel="stylesheet" href="css/solid.min.css">
	<link rel="stylesheet" href="css/fontawesome.min.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="css/main.css" class="color-switcher-link">
	<link rel="stylesheet" href="css/shop.css" class="color-switcher-link">
	<script src="js/vendor/modernizr-custom.js"></script>

	<!--[if lt IE 9]>
		<script src="js/vendor/html5shiv.min.js"></script>
		<script src="js/vendor/respond.min.js"></script>
		<script src="js/vendor/jquery-1.12.4.min.js"></script>
	<![endif]-->

</head>

<body>
	<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->


<div id="footer"></div>

			<div class="footer_before">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 animate" data-animation="fadeInUp">
							<ul class="bottom-includes">
								<li>
									<img src="images/logo_footer.png" alt="">
								</li>
								<li>
									<a href="#" class="fa fa-twitter" title="facebook">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-youtube-play" title="youtube-play">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-facebook" title="twitter">Connect on Facebook</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<footer class="page_footer ds s-py-50">
				<div class="container">
					<div class="row">
						<div class="divider-20 d-none d-xl-block"></div>

						<div class="col-12 col-sm-6 col-md-3 animate" data-animation="fadeInUp">
							<div class="widget widget_service">

								<h5 class="widget-title">My services</h5>

								<ul>
									<li>
										<a href="service-single.html">Parental Controls</a>
									</li>
									<li>
										<a href="service-single.html">Check Email</a>
									</li>
									<li>
										<a href="service-single.html">Check Voicemail </a>
									</li>
									<li>
										<a href="service-single.html">Manage Your Plan</a>
									</li>
									<li>
										<a href="service-single.html">Group Counseling</a>
									</li>

								</ul>
							</div>

							<div class="divider-30 divider-md-0"></div>
						</div>

						<div class="col-12 col-sm-6 col-md-3 animate" data-animation="fadeInUp">
							<div class="widget widget_archive">

								<h5 class="widget-title">My account</h5>

								<ul>
									<li>
										<a href="service-single.html">Pay Bill</a>
									</li>
									<li>
										<a href="service-single.html">Manage My Account</a>
									</li>
									<li>
										<a href="service-single.html">Manage Users & Alerts</a>
									</li>
									<li>
										<a href="service-single.html">Move Services</a>
									</li>
									<li>
										<a href="service-single.html">Cable Customer Agreement</a>
									</li>
								</ul>
							</div>

							<div class="divider-30 divider-sm-0"></div>
						</div>

						<div class="col-12 col-sm-6 col-md-3 animate" data-animation="fadeInUp">
							<div class="widget widget_archive">

								<h3 class="widget-title">Support</h3>

								<ul>
									<li>
										<a href="service-single.html">Comcast Customer Service</a>
									</li>
									<li>
										<a href="service-single.html">Bill & Payments </a>
									</li>
									<li>
										<a href="service-single.html">Contact Us </a>
									</li>
									<li>
										<a href="service-single.html">Support Forums</a>
									</li>
									<li>
										<a href="service-single.html">Comcast Customer Service</a>
									</li>

								</ul>
							</div>

							<div class="divider-30 divider-md-0"></div>
						</div>

						<div class="col-12 col-sm-6 col-md-3 animate" data-animation="fadeInUp">
							<div class="widget widget_mailchimp">

								<h5 class="widget-title">Subscribe us</h5>

								<form class="signup" action="http://webdesign-finder.com/">

									<input id="mailchimp_email" name="email" type="email" class="form-control mailchimp_email" placeholder="Your email">

									<button type="submit" class="search-submit">
										<i class="fa fa-envelope"></i>
									</button>
									<div class="response"></div>
								</form>

							</div>
						</div>

					</div>
				</div>
			</footer>

			<section class="page_copyright ds s-py-20">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-md-12">
							<p><span class="copyright_year">&copy;</span> Copyright 2019 All Rights</p>
						</div>
					</div>
				</div>
			</section>

		 


	<script src="js/compressed.js"></script>
	<script src="js/main.js"></script>
	<script src="js/switcher.js"></script>
    </body>
</html>
