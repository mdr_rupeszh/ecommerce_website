<?php include("header.php"); ?>

			<section class="page_title cover-background padding-mobile cs s-py-60 s-py-md-80 s-pt-xl-100 s-pb-xl-115">
				<div class="container">
					<div class="row">


						<div class="col-md-12">
							<h1 class="bold">Password Reset</h1>
							<ul class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.html">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Shop</a>
								</li>
								<li class="breadcrumb-item active">
									Password Reset
								</li>
							</ul>
						</div>


					</div>
				</div>
			</section>


			<!--eof topline-->


			<section class="ls s-py-50">
				<div class="container">
					<div class="row">

						<div class="d-none d-lg-block divider-70"></div>

						<main class="col-lg-12">
							<form method="post" class="woocommerce-ResetPassword lost_reset_password">

								<p>Lost your password? Please enter your username or email address. You will receive a link to create a new password
									via email.</p>

								<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
									<label for="user_login">Username or email</label>
									<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="user_login" id="user_login">
								</p>

								<div class="clear"></div>


								<p class="woocommerce-form-row form-row">
									<input type="submit" class="woocommerce-Button button" value="Reset password">
								</p>

							</form>


						</main>

						<div class="d-none d-lg-block divider-70"></div>
					</div>

				</div>
			</section>

			<div class="footer_before">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 animate" data-animation="fadeInUp">
							<ul class="bottom-includes">
								<li>
									<img src="images/logo_footer.png" alt="">
								</li>
								<li>
									<a href="#" class="fa fa-twitter" title="facebook">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-youtube-play" title="youtube-play">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-facebook" title="twitter">Connect on Facebook</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<footer class="page_footer ds s-pt-35 s-pb-45">
				<div class="container">
					<div class="row">
						<div class="divider-20 d-none d-xl-block"></div>

						<div class="col-12 col-sm-6 col-md-3 animate" data-animation="fadeInUp">
							<div class="widget widget_service">

								<h5 class="widget-title">My services</h5>

								<ul>
									<li>
										<a href="service-single.html">Parental Controls</a>
									</li>
									<li>
										<a href="service-single.html">Check Email</a>
									</li>
									<li>
										<a href="service-single.html">Check Voicemail </a>
									</li>
									<li>
										<a href="service-single.html">Manage Your Plan</a>
									</li>
									<li>
										<a href="service-single.html">Group Counseling</a>
									</li>

								</ul>
							</div>

							<div class="divider-30 divider-md-0"></div>
						</div>

						<div class="col-12 col-sm-6 col-md-3 animate" data-animation="fadeInUp">
							<div class="widget widget_archive">

								<h5 class="widget-title">My account</h5>

								<ul>
									<li>
										<a href="service-single.html">Pay Bill</a>
									</li>
									<li>
										<a href="service-single.html">Manage My Account</a>
									</li>
									<li>
										<a href="service-single.html">Manage Users & Alerts</a>
									</li>
									<li>
										<a href="service-single.html">Move Services</a>
									</li>
									<li>
										<a href="service-single.html">Cable Customer Agreement</a>
									</li>
								</ul>
							</div>

							<div class="divider-30 divider-sm-0"></div>
						</div>

						<div class="col-12 col-sm-6 col-md-3 animate" data-animation="fadeInUp">
							<div class="widget widget_archive">

								<h3 class="widget-title">Support</h3>

								<ul>
									<li>
										<a href="service-single.html">Comcast Customer Service</a>
									</li>
									<li>
										<a href="service-single.html">Bill & Payments </a>
									</li>
									<li>
										<a href="service-single.html">Contact Us </a>
									</li>
									<li>
										<a href="service-single.html">Support Forums</a>
									</li>
									<li>
										<a href="service-single.html">Comcast Customer Service</a>
									</li>

								</ul>
							</div>

							<div class="divider-30 divider-md-0"></div>
						</div>

						<div class="col-12 col-sm-6 col-md-3 animate" data-animation="fadeInUp">
							<div class="widget widget_mailchimp">

								<h5 class="widget-title">Subscribe us</h5>

								<form class="signup" action="http://webdesign-finder.com/">

									<input id="mailchimp_email" name="email" type="email" class="form-control mailchimp_email" placeholder="Your email">

									<button type="submit" class="search-submit">
										<i class="fa fa-envelope"></i>
									</button>
									<div class="response"></div>
								</form>

							</div>
						</div>

					</div>
				</div>
			<?php include("footer.php"); ?>