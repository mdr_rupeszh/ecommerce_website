
<?php include("header.php"); ?>
			<section class="page_title cover-background padding-mobile cs s-py-60 s-py-md-80 s-pt-xl-100 s-pb-xl-115">
				<div class="container">
					<div class="row">


						<div class="col-md-12">
							<h1 class="bold">Shopping Cart</h1>
							<ul class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.html">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Shop</a>
								</li>
								<li class="breadcrumb-item active">
									Shopping Cart
								</li>
							</ul>
						</div>


					</div>
				</div>
			</section>


			<!--eof topline-->


			<section class="ls s-py-50">
				<div class="container">
					<div class="row">

						<div class="d-none d-lg-block divider-70"></div>

						<main class="col-lg-12">
							<div class="woocommerce-message">
								"Premium quality" removed. <a href="#">Undo?</a>
							</div>

							<form class="woocommerce-cart-form" action="http://webdesign-finder.com/" method="post">

								<table class="shop_table shop_table_responsive cart">
									<thead>
										<tr>
											<th class="product-remove">&nbsp;</th>
											<th class="product-thumbnail">&nbsp;</th>
											<th class="product-name">Product</th>
											<th class="product-price">Price</th>
											<th class="product-quantity">Quantity</th>
											<th class="product-subtotal">Total</th>
										</tr>
									</thead>
									<tbody>

										<tr class="cart_item">

											<td class="product-remove">
												<a href="#" class="remove" aria-label="Remove this item" data-product_id="73" data-product_sku=""></a>
											</td>

											<td class="product-thumbnail">
												<a href="shop-product-right.html">
													<img width="180" height="180" src="images/shop/01.jpg" class="" alt="">
												</a>
											</td>

											<td class="product-name" data-title="Product">
												<a href="shop-product-right.html">Premium Quality</a>
											</td>

											<td class="product-price" data-title="Price">
												<span class="amount">
													<span>$</span>12.00
												</span>
											</td>

											<td class="product-quantity" data-title="Quantity">
												<div class="quantity">
													<input type="number" class="input-text qty text" step="1" min="0" max="1000" name="cart[qty]" value="1" title="Qty" size="4">
												</div>
											</td>

											<td class="product-subtotal" data-title="Total">
												<span class="amount">
													<span>$</span>12.00
												</span>
											</td>
										</tr>
										<tr class="cart_item">

											<td class="product-remove">
												<a href="#" class="remove" aria-label="Remove this item" data-product_id="76" data-product_sku=""></a>
											</td>

											<td class="product-thumbnail">
												<a href="shop-product-right.html">
													<img width="180" height="180" src="images/shop/02.jpg" class="" alt="">
												</a>
											</td>

											<td class="product-name" data-title="Product">
												<a href="shop-product-right.html">Woo Ninja</a>
											</td>

											<td class="product-price" data-title="Price">
												<span class="amount">
													<span>$</span>15.00
												</span>
											</td>

											<td class="product-quantity" data-title="Quantity">
												<div class="quantity">
													<input type="number" class="input-text qty text" step="1" min="0" max="1000" name="cart[qty]" value="1" title="Qty" size="4">
												</div>
											</td>

											<td class="product-subtotal" data-title="Total">
												<span class="amount">
													<span>$</span>15.00
												</span>
											</td>
										</tr>
										<tr class="cart_item">

											<td class="product-remove">
												<a href="#" class="remove" aria-label="Remove this item" data-product_id="90" data-product_sku=""></a>
											</td>

											<td class="product-thumbnail">
												<a href="shop-product-right.html">
													<img width="180" height="180" src="images/shop/03.jpg" class="" alt="">
												</a>
											</td>

											<td class="product-name" data-title="Product">
												<a href="shop-product-right.html">Woo Album #3</a>
											</td>

											<td class="product-price" data-title="Price">
												<span class="amount">
													<span>$</span>9.00
												</span>
											</td>

											<td class="product-quantity" data-title="Quantity">
												<div class="quantity">
													<input type="number" class="input-text qty text" step="1" min="0" max="1000" name="cart[qty]" value="2" title="Qty" size="4">
												</div>
											</td>

											<td class="product-subtotal" data-title="Total">
												<span class="amount">
													<span>$</span>18.00
												</span>
											</td>
										</tr>


										<tr>
											<td colspan="6" class="actions">

												<div class="coupon">
													<label for="coupon_code">Coupon:</label>
													<input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="Coupon code">
													<input type="submit" class="button" name="apply_coupon" value="Apply coupon">
												</div>

												<input type="submit" class="button" name="update_cart" value="Update cart" disabled="">

											</td>
										</tr>

									</tbody>
								</table>
							</form>

							<div class="cart-collaterals">

								<div class="cross-sells">

									<h2>You may be interested in…</h2>

									<ul class="products">

										<li class="product type-product">
											<div class="product-inner">
												<a href="shop-product-right.html">
													<img src="images/shop/04.jpg" class="" alt="">
													<h2>Woo Ninja 2</h2>
													<div class="star-rating">
														<span style="width:80%">Rated <strong class="rating">4.00
                            </strong> out of 5</span>
													</div>
													<span class="price">
														<span class="amount">
															<span>$</span>34.00
														</span>
													</span>
												</a>

											</div>
										</li>

										<li class="product type-product">
											<div class="product-inner">
												<a href="shop-product-right.html">
													<img src="images/shop/05.jpg" class="" alt="">
													<h2>Woo Ninja</h2>
													<span class="price">
														<span class="amount">
															<span>$</span>20.00
														</span>
													</span>
												</a>

											</div>
										</li>


										<li class="product type-product">
											<div class="product-inner">
												<a href="shop-product-right.html">
													<img src="images/shop/06.jpg" class="" alt="">
													<h2>Woo Ninja</h2>
													<div class="star-rating">
														<span style="width:90%">Rated <strong class="rating">4.50
                            </strong> out of 5</span>
													</div>
													<span class="price">
														<span class="amount">
															<span>$</span>35.00
														</span>
													</span>
												</a>

											</div>
										</li>


										<li class="product type-product">
											<div class="product-inner">
												<a href="shop-product-right.html">
													<img src="images/shop/01.jpg" class="" alt="">
													<h2>Woo Ninja 5</h2>
													<div class="star-rating">
														<span style="width:80%">Rated <strong class="rating">4.00
                            </strong> out of 5</span>
													</div>
													<span class="price">
														<span class="amount">
															<span>$</span>39.00
														</span>
													</span>
												</a>

											</div>
										</li>

										<li class="product type-product">
											<div class="product-inner">
												<a href="shop-product-right.html">
													<img src="images/shop/02.jpg" class="" alt="">
													<h2>Woo Ninja</h2>
													<span class="price">
														<span class="amount">
															<span>$</span>20.00
														</span>
													</span>
												</a>
											</div>
										</li>


									</ul>

								</div>

								<div class="cart_totals ">


									<h2>Cart totals</h2>

									<table class="shop_table shop_table_responsive">

										<tbody>
											<tr class="cart-subtotal">
												<th>Subtotal</th>
												<td data-title="Subtotal">
													<span class="amount">
														<span>$</span>45.00
													</span>
												</td>
											</tr>


											<tr class="order-total">
												<th>Total</th>
												<td data-title="Total">
													<strong>
                        <span class="amount">
                            <span>$</span>45.00
                        </span>
                    </strong>
												</td>
											</tr>


										</tbody>
									</table>

									<div class="wc-proceed-to-checkout">

										<a href="shop-checkout.html" class="checkout-button button alt wc-forward">
											Proceed to checkout</a>
									</div>


								</div>
							</div>


						</main>

						<div class="d-none d-lg-block divider-70"></div>
					</div>

				</div>
			</section>

		<?php include("footer.php"); ?>