<?php include("header.php"); ?>

			<section class="page_title cover-background padding-mobile cs s-py-60 s-py-md-80 s-pt-xl-100 s-pb-xl-115">
				<div class="container">
					<div class="row">


						<div class="col-md-12">
							<h1 class="bold">Downloads</h1>
							<ul class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.html">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Shop</a>
								</li>
								<li class="breadcrumb-item active">
									Downloads
								</li>
							</ul>
						</div>


					</div>
				</div>
			</section>


			<!--eof topline-->


			<section class="ls s-py-50">
				<div class="container">
					<div class="row">

						<div class="d-none d-lg-block divider-70"></div>

						<main class="col-lg-12">
							<article>
								<header class="entry-header">
									<h1 class="entry-title">Downloads</h1>
									<span class="edit-link">
										<a class="post-edit-link" href="#">Edit<span class="screen-reader-text"> "My account"</span>
										</a>
									</span>
								</header>
								<!-- .entry-header -->
								<div class="entry-content">
									<div class="woocommerce">
										<nav class="woocommerce-MyAccount-navigation">
											<ul>
												<li>
													<a href="shop-account-dashboard.html">Dashboard</a>
												</li>
												<li>
													<a href="shop-account-orders.html">Orders</a>
												</li>
												<li class="is-active">
													<a href="shop-account-downloads.html">Downloads</a>
												</li>
												<li>
													<a href="shop-account-addresses.html">Addresses</a>
												</li>
												<li>
													<a href="shop-account-details.html">Account details</a>
												</li>
												<li>
													<a href="shop-account-login.html">Logout</a>
												</li>
											</ul>
										</nav>


										<div class="woocommerce-MyAccount-content">

											<div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
												<a class="woocommerce-Button button" href="shop-right.html">
													Go shop
												</a>
												No downloads available yet.
											</div>

											<table class="woocommerce-MyAccount-downloads shop_table shop_table_responsive">
												<thead>
													<tr>
														<th class="download-product">
															<span class="nobr">Product</span>
														</th>
														<th class="download-remaining">
															<span class="nobr">Downloads remaining</span>
														</th>
														<th class="download-expires">
															<span class="nobr">Expires</span>
														</th>
														<th class="download-file">
															<span class="nobr">File</span>
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="download-product" data-title="Product">
															<a href="#">
																Downloadable Product #1
															</a>
														</td>
														<td class="download-remaining" data-title="Downloads remaining">
															∞
														</td>
														<td class="download-expires" data-title="Expires">
															Never
														</td>
														<td class="download-file" data-title="File">
															<a href="#" class="woocommerce-MyAccount-downloads-file">
																File
															</a>
														</td>
													</tr>
													<tr>
														<td class="download-product" data-title="Product">
															<a href="#">
																Product Downloadable #2
															</a>
														</td>
														<td class="download-remaining" data-title="Downloads remaining">
															∞
														</td>
														<td class="download-expires" data-title="Expires">
															Never
														</td>
														<td class="download-file" data-title="File">
															<a href="#" class="woocommerce-MyAccount-downloads-file">
																File
															</a>
														</td>
													</tr>
													<tr>
														<td class="download-product" data-title="Product">
															<a href="#">
																Downloadable Product #3
															</a>
														</td>
														<td class="download-remaining" data-title="Downloads remaining">
															∞
														</td>
														<td class="download-expires" data-title="Expires">
															Never
														</td>
														<td class="download-file" data-title="File">
															<a href="#" class="woocommerce-MyAccount-downloads-file">
																File
															</a>
														</td>
													</tr>
												</tbody>
											</table>

										</div>
									</div>
								</div>
								<!-- .entry-content -->
							</article>


						</main>

						<div class="d-none d-lg-block divider-70"></div>
					</div>

				</div>
			</section>

			<div class="footer_before">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 animate" data-animation="fadeInUp">
							<ul class="bottom-includes">
								<li>
									<img src="images/logo_footer.png" alt="">
								</li>
								<li>
									<a href="#" class="fa fa-twitter" title="facebook">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-youtube-play" title="youtube-play">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-facebook" title="twitter">Connect on Facebook</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
	<?php include("footer.php"); ?>