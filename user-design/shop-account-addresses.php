
<?php include("header.php"); ?>

			<section class="page_title cover-background padding-mobile cs s-py-60 s-py-md-80 s-pt-xl-100 s-pb-xl-115">
				<div class="container">
					<div class="row">


						<div class="col-md-12">
							<h1 class="bold">Shop - Addresses</h1>
							<ul class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.html">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Shop</a>
								</li>
								<li class="breadcrumb-item active">
									Shop - Addresses
								</li>
							</ul>
						</div>


					</div>
				</div>
			</section>


			<!--eof topline-->


			<section class="ls s-py-50">
				<div class="container">
					<div class="row">

						<div class="d-none d-lg-block divider-70"></div>

						<main class="col-lg-12">
							<article>
								<header class="entry-header">
									<h1 class="entry-title">Addresses</h1>

								</header>
								<!-- .entry-header -->
								<div class="entry-content">
									<div class="woocommerce">
										<nav class="woocommerce-MyAccount-navigation">
											<ul>
												<li>
													<a href="shop-account-dashboard.html">Dashboard</a>
												</li>
												<li>
													<a href="shop-account-orders.html">Orders</a>
												</li>
												<li>
													<a href="shop-account-downloads.html">Downloads</a>
												</li>
												<li class="is-active">
													<a href="shop-account-addresses.html">Addresses</a>
												</li>
												<li>
													<a href="shop-account-details.html">Account details</a>
												</li>
												<li>
													<a href="shop-account-login.html">Logout</a>
												</li>
											</ul>
										</nav>

										<div class="woocommerce-MyAccount-content">
											<p>
												The following addresses will be used on the checkout page by default.</p>

											<div class="u-columns woocommerce-Addresses col2-set addresses">


												<div class="u-column1 col-1 woocommerce-Address">
													<header class="woocommerce-Address-title title">
														<h3>Billing address</h3>
														<a href="shop-account-address-edit.html" class="edit">Edit</a>
													</header>
													<address>
														John Doe
														<br>
														Baker Street, 231
														<br>
														London
														<br>
														Great Britain
														<br>
														12000
													</address>
												</div>

												<div class="u-column2 col-2 woocommerce-Address">
													<header class="woocommerce-Address-title title">
														<h3>Shipping address</h3>
														<a href="shop-account-address-edit.html" class="edit">Edit</a>
													</header>
													<address>
														You have not set up this type of address yet.
													</address>
												</div>


											</div>

										</div>
									</div>
								</div>
								<!-- .entry-content -->
							</article>

						</main>

						<div class="d-none d-lg-block divider-70"></div>
					</div>

				</div>
			</section>

			<div class="footer_before">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 animate" data-animation="fadeInUp">
							<ul class="bottom-includes">
								<li>
									<img src="images/logo_footer.png" alt="">
								</li>
								<li>
									<a href="#" class="fa fa-twitter" title="facebook">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-youtube-play" title="youtube-play">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-facebook" title="twitter">Connect on Facebook</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<?php include("footer.php"); ?>