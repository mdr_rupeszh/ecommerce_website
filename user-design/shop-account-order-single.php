<?php include("header.php"); ?>

			<section class="page_title cover-background padding-mobile cs s-py-60 s-py-md-80 s-pt-xl-100 s-pb-xl-115">
				<div class="container">
					<div class="row">


						<div class="col-md-12">
							<h1 class="bold">Shop Single Order</h1>
							<ul class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.html">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Shop</a>
								</li>
								<li class="breadcrumb-item active">
									Shop Single Order
								</li>
							</ul>
						</div>


					</div>
				</div>
			</section>


			<!--eof topline-->


			<section class="ls s-py-50">
				<div class="container">
					<div class="row">

						<div class="d-none d-lg-block divider-70"></div>

						<main class="col-lg-12">
							<article id="post-1708" class="post-1708 page type-page status-publish hentry">
								<header class="entry-header">
									<h1 class="entry-title">Order #1722</h1>
								</header>
								<!-- .entry-header -->
								<div class="entry-content">
									<div class="woocommerce">
										<nav class="woocommerce-MyAccount-navigation">
											<ul>
												<li>
													<a href="shop-account-dashboard.html">Dashboard</a>
												</li>
												<li class="is-active">
													<a href="shop-account-orders.html">Orders</a>
												</li>
												<li>
													<a href="shop-account-downloads.html">Downloads</a>
												</li>
												<li>
													<a href="shop-account-addresses.html">Addresses</a>
												</li>
												<li>
													<a href="shop-account-details.html">Account details</a>
												</li>
												<li>
													<a href="shop-account-login.html">Logout</a>
												</li>
											</ul>
										</nav>


										<div class="woocommerce-MyAccount-content">
											<p>Order #
												<mark class="order-number">1722</mark>
												was placed on
												<mark class="order-date">March 8, 2018</mark>
												and is currently
												<mark class="order-status">Completed</mark>
												.
											</p>


											<section class="woocommerce-order-details">

												<h2 class="woocommerce-order-details__title">Order details</h2>

												<table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

													<thead>
														<tr>
															<th class="woocommerce-table__product-name product-name">Product</th>
															<th class="woocommerce-table__product-table product-total">Total</th>
														</tr>
													</thead>

													<tbody>
														<tr class="woocommerce-table__line-item order_item">

															<td class="woocommerce-table__product-name product-name">
																<a href="shop-product-right.html">Downloadable Product #1</a>
																<strong class="product-quantity">× 1</strong>
																<ul class="wc-item-downloads">
																	<li>
																		<strong class="wc-item-download-label">Download:</strong>
																		<a href="#">File</a>
																	</li>
																	<li>
																		<strong class="wc-item-download-label">Download:</strong>
																		<a href="#">File</a>
																	</li>
																</ul>
															</td>

															<td class="woocommerce-table__product-total product-total">
																<span class="woocommerce-Price-amount amount">
																	<span class="woocommerce-Price-currencySymbol">$</span>12.00
																</span>
															</td>

														</tr>

													</tbody>

													<tfoot>
														<tr>
															<th scope="row">Subtotal:</th>
															<td>
																<span class="woocommerce-Price-amount amount">
																	<span class="woocommerce-Price-currencySymbol">$</span>12.00</span>
															</td>
														</tr>
														<tr>
															<th scope="row">Payment method:</th>
															<td>Cash on delivery</td>
														</tr>
														<tr>
															<th scope="row">Total:</th>
															<td>
																<span class="woocommerce-Price-amount amount">
																	<span class="woocommerce-Price-currencySymbol">$</span>12.00</span>
															</td>
														</tr>
													</tfoot>

												</table>


												<p class="order-again">
													<a href="http://webdesign-finder.com/my-account/view-order/1722/?order_again=1722&amp;_wpnonce=8edf688805" class="button">Order
														again</a>
												</p>


												<section class="woocommerce-customer-details">

													<h2>Customer details</h2>

													<table class="woocommerce-table woocommerce-table--customer-details shop_table customer_details">


														<tbody>
															<tr>
																<th>Email:</th>
																<td>admin@test.com</td>
															</tr>

															<tr>
																<th>Phone:</th>
																<td>+1300551555</td>
															</tr>


														</tbody>
													</table>


													<h3 class="woocommerce-column__title">Billing address</h3>

													<address>
														John Doe<br>Baker Street, 231<br>London<br>Great Britain<br>12000
													</address>


												</section>

											</section>
										</div>
									</div>
								</div>
								<!-- .entry-content -->
							</article>

						</main>

						<div class="d-none d-lg-block divider-70"></div>
					</div>

				</div>
			</section>

			<div class="footer_before">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 animate" data-animation="fadeInUp">
							<ul class="bottom-includes">
								<li>
									<img src="images/logo_footer.png" alt="">
								</li>
								<li>
									<a href="#" class="fa fa-twitter" title="facebook">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-youtube-play" title="youtube-play">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-facebook" title="twitter">Connect on Facebook</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<?php include("footer.php"); ?>