<?php include("header.php"); ?>

			<section class="page_title cover-background padding-mobile cs s-py-60 s-py-md-80 s-pt-xl-100 s-pb-xl-115">
				<div class="container">
					<div class="row">


						<div class="col-md-12">
							<h1 class="bold">Shop with Right Sidebar</h1>
							<ul class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.html">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Shop</a>
								</li>
								<li class="breadcrumb-item active">
									Shop with Right Sidebar
								</li>
							</ul>
						</div>


					</div>
				</div>
			</section>


			<!--eof topline-->


			<section class="ls s-py-60 s-py-md-80 s-py-xl-150 c-gutter-60">
				<div class="container">
					<div class="row">


						<main class="col-lg-8 col-xl-9">
							<div class="columns-3">
								<div class="products-selection">
									<span class="toggle_view">
										<a class="grid active" href="#"><i class="fa fa-th-large"></i></a>
										<a class="full" href="#"><i class="ico icon-menu"></i></a>
									</span>
									<span>Sort by:</span>
									<form class="woocommerce-ordering" method="get">
										<select name="orderby" class="orderby">
											<option value="menu_order" selected="selected">Default sorting</option>
											<option value="popularity">Sort by popularity</option>
											<option value="rating">Sort by average rating</option>
											<option value="date">Sort by newness</option>
											<option value="price">Sort by price: low to high</option>
											<option value="price-desc">Sort by price: high to low</option>
										</select>
									</form>
									<p class="woocommerce-result-count">
										Showing all 23 results
									</p>
								</div>

								<ul class="products">


									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">

												<span class="onsale">-15%</span>
												<img src="images/shop/01.jpg" alt="">

												<h2>Mobile Router 02</h2>

												<span class="price">
													<del>
														<span class="woocommerce-Price-amount amount">
															<span class="woocommerce-Price-currencySymbol">$</span>25.00
														</span>
													</del>
													<ins>
														<span class="woocommerce-Price-amount amount">
															<span class="woocommerce-Price-currencySymbol">$</span>15.00
														</span>
													</ins>
												</span>

											</a>
										</div>
									</li>


									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">
												<span class="onsale">-25%</span>
												<img src="images/shop/02.jpg" alt="">
												<h2>SM Router 01</h2>
												<span class="price">
													<del>
														<span class="woocommerce-Price-amount amount">
															<span class="woocommerce-Price-currencySymbol">$</span>28.00
														</span>
													</del>
													<ins>
														<span class="woocommerce-Price-amount amount">
															<span class="woocommerce-Price-currencySymbol">$</span>20.00
														</span>
													</ins>
												</span>
											</a>

										</div>
									</li>


									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">
												<span class="onsale">-15%</span>
												<img src="images/shop/03.jpg" alt="">
												<h2>Extender 0402</h2>

												<span class="price">
													<del>
														<span class="woocommerce-Price-amount amount">
															<span class="woocommerce-Price-currencySymbol">$</span>20.00
														</span>
													</del>
													<ins>
														<span class="woocommerce-Price-amount amount">
															<span class="woocommerce-Price-currencySymbol">$</span>12.00
														</span>
													</ins>
												</span>
											</a>
										</div>
									</li>


									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">
												<img src="images/shop/04.jpg" alt="">
												<h2>Switch Port 303</h2>
												<span class="price">
													<span>
														<span>$</span>20.00
													</span>
												</span>
											</a>

										</div>
									</li>


									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">
												<img src="images/shop/05.jpg" alt="">
												<h2>Switch Port 305</h2>

												<span class="price">
													<span>
														<span>$</span>35.00
													</span>
												</span>
											</a>

										</div>
									</li>


									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">
												<img src="images/shop/06.jpg" alt="">
												<h2>Path Switcher</h2>

												<span class="price">
													<span>
														<span>$</span>35.00
													</span>
												</span>
											</a>

										</div>
									</li>


									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">
												<img src="images/shop/07.jpg" alt="">
												<h2>SM Router 01</h2>
												<span class="price">
													<span>
														<span>$</span>18.00
													</span>
												</span>
											</a>

										</div>
									</li>


									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">
												<img src="images/shop/08.jpg" alt="">
												<h2>Extender 0402</h2>

												<span class="price">
													<span>
														<span>$</span>35.00
													</span>
												</span>
											</a>
										</div>
									</li>


									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">
												<img src="images/shop/09.jpg" alt="">
												<h2>Switch Port 303</h2>
												<span class="price">
													<span>
														<span>$</span>20.00
													</span>
												</span>
											</a>

										</div>
									</li>


									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">
												<img src="images/shop/10.jpg" alt="">
												<h2>Switch Port 305</h2>

												<span class="price">
													<span>
														<span>$</span>35.00
													</span>
												</span>
											</a>

										</div>
									</li>


									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">
												<img src="images/shop/11.jpg" alt="">
												<h2>Path Switcher</h2>

												<span class="price">
													<span>
														<span>$</span>35.00
													</span>
												</span>
											</a>

										</div>
									</li>
									<li class="product">
										<div class="product-inner">
											<a href="shop-product-right.html">
												<img src="images/shop/12.jpg" alt="">
												<h2>Extender 0402</h2>

												<span class="price">
													<span>
														<span>$</span>35.00
													</span>
												</span>
											</a>
										</div>
									</li>
								</ul>
							</div>
							<!-- columns 2 -->

							<nav class="woocommerce-pagination">
								<ul class="page-numbers">
									<li>
										<span class="page-numbers current">1</span>
									</li>
									<li>
										<a class="page-numbers" href="shop-right.html">2</a>
									</li>
									<li>
										<a class="next page-numbers" href="shop-right.html">→</a>
									</li>
								</ul>
							</nav>

						</main>

						<aside class="col-lg-4 col-xl-3">
							<div class="widget widget_search">
								<h3 class="widget-title">Search</h3>
								<form role="search" method="get" class="search-form" action="http://webdesign-finder.com/">
									<label for="search-form-widget">
										<span class="screen-reader-text">Search for:</span>
									</label>
									<input type="search" id="search-form-widget" class="search-field" placeholder="Search" value="" name="search">
									<button type="submit" class="search-submit">
										<span class="screen-reader-text">Search</span>
									</button>
								</form>
							</div>


							<div class="widget woocommerce widget_product_categories">
								<h3 class="widget-title">Categories</h3>
								<ul class="product-categories">
									<li class="cat-item cat-parent">
										<a href="shop-right.html">Clothing</a>
										<span class="count">(12)</span>
										<ul class="children">
											<li class="cat-item">
												<a href="shop-right.html">Hoodies</a>
												<span class="count">(46)</span>
											</li>
											<li class="cat-item">
												<a href="shop-right.html">T-shirts</a>
												<span class="count">(165)</span>
											</li>
										</ul>
									</li>
									<li class="cat-item cat-parent">
										<a href="shop-right.html">Music</a>
										<span class="count">(16)</span>
										<ul class="children">
											<li class="cat-item">
												<a href="shop-right.html">Albums</a>
												<span class="count">(24)</span>
											</li>
											<li class="cat-item">
												<a href="shop-right.html">Singles</a>
												<span class="count">(2)</span>
											</li>
										</ul>
									</li>
									<li class="cat-item">
										<a href="shop-right.html">Posters</a>
										<span class="count">(45)</span>
									</li>
								</ul>
							</div>


							<div class="widget woocommerce widget_price_filter">

								<h3 class="widget-title">Price Filter</h3>

								<form method="get" action="http://webdesign-finder.com/">
									<div class="price_slider_wrapper">
										<div class="price_slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" style="">
											<div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 10%; width: 70%;">

											</div>
											<span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 10%;">

											</span>
											<span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 80%;">

											</span>
										</div>
										<div class="price_slider_amount">
											<input type="text" id="min_price" name="min_price" value="" data-min="20" placeholder="Min price" style="display: none;">
											<input type="text" id="max_price" name="max_price" value="" data-max="30" placeholder="Max price" style="display: none;">

											<div class="price_label" style="">
												<span class="from">$21</span> - <span class="to">$28</span>
											</div>
											<button type="submit" class="button">Filter</button>
											<div class="clear"></div>

										</div>
									</div>
								</form>
							</div>


							<div class="widget woocommerce widget_shopping_cart">

								<h3 class="widget-title">Shopping Cart</h3>

								<div class="widget_shopping_cart_content">

									<ul class="woocommerce-mini-cart cart_list product_list_widget ">
										<li class="woocommerce-mini-cart-item mini_cart_item">
											<a href="#" class="remove" aria-label="Remove this item" data-product_id="73" data-product_sku=""></a>
											<a href="shop-product-right.html">
												<img src="images/shop/01.jpg" alt="">IS Router 002
											</a>
											<span class="quantity">1 ×
												<span class="woocommerce-Price-amount amount">
													<span class="woocommerce-Price-currencySymbol">$</span>
													12.00
												</span>
											</span>
										</li>
										<li class="woocommerce-mini-cart-item mini_cart_item">
											<a href="#" class="remove" aria-label="Remove this item" data-product_id="76" data-product_sku=""></a>
											<a href="shop-product-right.html">
												<img src="images/shop/02.jpg" alt="">Remote Cntrl
											</a>
											<span class="quantity">1 ×
												<span class="woocommerce-Price-amount amount">
													<span class="woocommerce-Price-currencySymbol">$</span>
													15.00
												</span>
											</span>
										</li>
									</ul>

									<p class="woocommerce-mini-cart__total total">
										<strong>Subtotal:</strong>
										<span class="woocommerce-Price-amount amount">
											<span class="woocommerce-Price-currencySymbol">$</span>
											27.00
										</span>
									</p>

									<p class="woocommerce-mini-cart__buttons buttons">
										<a href="shop-cart.html" class="button wc-forward">View cart</a>
										<a href="shop-checkout.html" class="button checkout wc-forward ">Checkout</a>
									</p>
								</div>
							</div>


						</aside>


					</div>

				</div>
			</section>

			<div class="footer_before">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 animate" data-animation="fadeInUp">
							<ul class="bottom-includes">
								<li>
									<img src="images/logo_footer.png" alt="">
								</li>
								<li>
									<a href="#" class="fa fa-twitter" title="facebook">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-youtube-play" title="youtube-play">Find tutorials and demos</a>
								</li>
								<li>
									<a href="#" class="fa fa-facebook" title="twitter">Connect on Facebook</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<?php include("footer.php"); ?>