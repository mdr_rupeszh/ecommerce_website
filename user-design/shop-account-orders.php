<?php include("header.php"); ?>

			<section class="page_title cover-background padding-mobile cs s-py-60 s-py-md-80 s-pt-xl-100 s-pb-xl-115">
				<div class="container">
					<div class="row">


						<div class="col-md-12">
							<h1 class="bold">Shop Orders</h1>
							<ul class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.html">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Shop</a>
								</li>
								<li class="breadcrumb-item active">
									Shop Orders
								</li>
							</ul>
						</div>


					</div>
				</div>
			</section>


			<!--eof topline-->


			<section class="ls s-py-50">
				<div class="container">
					<div class="row">

						<div class="d-none d-lg-block divider-70"></div>

						<main class="col-lg-12">
							<article id="post-1708" class="post-1708 page type-page status-publish hentry">
								<header class="entry-header">
									<h1 class="entry-title">Orders</h1>
								</header><!-- .entry-header -->
								<div class="entry-content">
									<div class="woocommerce">
										<nav class="woocommerce-MyAccount-navigation">
											<ul>
												<li>
													<a href="shop-account-dashboard.html">Dashboard</a>
												</li>
												<li class="is-active">
													<a href="shop-account-orders.html">Orders</a>
												</li>
												<li>
													<a href="shop-account-downloads.html">Downloads</a>
												</li>
												<li>
													<a href="shop-account-addresses.html">Addresses</a>
												</li>
												<li>
													<a href="shop-account-details.html">Account details</a>
												</li>
												<li>
													<a href="shop-account-login.html">Logout</a>
												</li>
											</ul>
										</nav>


										<div class="woocommerce-MyAccount-content">

											<div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
												<a class="woocommerce-Button button" href="shop-right.html">
													Go shop </a>
												No order has been made yet.
											</div>

											<table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
												<thead>
													<tr>
														<th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number"><span class="nobr">Order</span></th>
														<th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Date</span></th>
														<th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Status</span></th>
														<th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-total"><span class="nobr">Total</span></th>
														<th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Actions</span></th>
													</tr>
												</thead>

												<tbody>
													<tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-processing order">
														<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Order">
															<a href="shop-account-order-single.html">
																#1719 </a>

														</td>
														<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Date">
															<time datetime="2018-03-06T08:55:39+00:00">March 6, 2018</time>

														</td>
														<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
															Processing
														</td>
														<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Total">
															<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>45.00</span> for 4 items
														</td>
														<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
															<a href="shop-account-order-single.html" class="woocommerce-button button view">View</a> </td>
													</tr>

													<tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-on-hold order">
														<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Order">
															<a href="shop-account-order-single.html">
																#106 </a>

														</td>
														<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Date">
															<time datetime="2018-03-22T13:34:43+00:00">March 22, 2018</time>

														</td>
														<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
															On hold
														</td>
														<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Total">
															<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>100.00</span> for 4 items
														</td>
														<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
															<a href="shop-account-order-single.html" class="woocommerce-button button view">View</a> </td>
													</tr>

												</tbody>
											</table>

										</div>
									</div>
								</div><!-- .entry-content -->
							</article>

						</main>

						<div class="d-none d-lg-block divider-70"></div>
					</div>

				</div>
			</section>

			<?php include("footer.php"); ?>