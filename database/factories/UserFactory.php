<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {

    return [
        'username' => $faker->word,
        'password' => $faker->word,
        'device_type' => $faker->word,
        'firstname' => $faker->word,
        'lastname' => $faker->word,
        'userid' => $faker->word,
        'tax_registration_number' => $faker->word,
        'address' => $faker->word,
        'postcode' => $faker->word,
        'city' => $faker->word,
        'county' => $faker->word,
        'state' => $faker->word,
        'country' => $faker->word,
        'phone' => $faker->word,
        'mobile' => $faker->word,
        'fax' => $faker->word,
        'email' => $faker->word,
        'email_verified_at' => $faker->date('Y-m-d H:i:s'),
        'verified' => $faker->randomDigitNotNull,
        'remember_token' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
