<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {                
        DB::table('permissions')->delete();
        $permissions=["users","roles","plans"];        
        foreach ($permissions as $permission) {
            $permission=Permission::create(['name' => $permission]);            
        }
        
        DB::table('roles')->delete();
        $roles=["Super Administrator","Administrator","Customer"];
        
        foreach ($roles as $role) {
            
            $role=Role::create(['name' => $role]);
            if($role->name=="Super Administrator"){
                $permissions= Permission::get();
                foreach ($permissions as $permission) {
                    $role->givePermissionTo($permission);
                }
            }
            
            
        }
        
        
        
        DB::table('users')->delete();
        $users = array(
            array(
                'id' => '1',                
                'name' => 'Administrator',                
                'password' => Hash::make('Nepal123'),
                'email' => 'admin@admin.com',                         
                'verified' => "1",                         
                'created_at' => date('Y-m-d H:i'),                         
                'updated_at' => date('Y-m-d H:i'),                         
            )
        );
        $user =new App\Models\User();        
        $user->firstname="E-Commerce";
        $user->lastname="Administrator";
        $user->password=Hash::make('Nepal123');
        $user->email="admin@admin.com"; 
        $user->username="admin@admin.com"; 
        $user->verified=1;
        $user->save();
        $user->assignRole('Super Administrator');
//        DB::table('users')->insert($users);
    }
}
