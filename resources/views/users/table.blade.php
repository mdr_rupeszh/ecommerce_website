<div class="table-responsive">
    <table class="table" id="users-table">
        <thead>
            <tr>
                <!--<th>Username</th>-->
                <!--<th>Password</th>-->
                <!--<th>Device Type</th>-->
                <th>Firstname</th>
                <th>Lastname</th>
                <!--<th>Userid</th>-->
                <!--<th>Tax Registration Number</th>-->
<!--                <th>Address</th>
                <th>Postcode</th>
                <th>City</th>
                <th>County</th>
                <th>State</th>
                <th>Country</th>
                <th>Phone</th>
                <th>Mobile</th>
                <th>Fax</th>
                <th>Email</th>
                <th>Email Verified At</th>-->
                <th>Verified</th>
                <!--<th>Remember Token</th>-->
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <!--<td>{!! $user->username !!}</td>-->
                <!--<td>{!! $user->password !!}</td>-->
                <!--<td>{!! $user->device_type !!}</td>-->
                <td>{!! $user->firstname !!}</td>
                <td>{!! $user->lastname !!}</td>
                <!--<td>{!! $user->userid !!}</td>-->
                <!--<td>{!! $user->tax_registration_number !!}</td>-->
<!--                <td>{!! $user->address !!}</td>
                <td>{!! $user->postcode !!}</td>
                <td>{!! $user->city !!}</td>
                <td>{!! $user->county !!}</td>
                <td>{!! $user->state !!}</td>
                <td>{!! $user->country !!}</td>
                <td>{!! $user->phone !!}</td>
                <td>{!! $user->mobile !!}</td>
                <td>{!! $user->fax !!}</td>
                <td>{!! $user->email !!}</td>
                <td>{!! $user->email_verified_at !!}</td>-->
                <td>{!! $user->verified !!}</td>
                <!--<td>{!! $user->remember_token !!}</td>-->
                <td>
                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
