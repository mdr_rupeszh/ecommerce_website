

@if(auth()->user()->hasPermissionTo('users'))
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-user"></i><span>Users</span></a>
</li>
@endif

@if(auth()->user()->hasPermissionTo('roles'))
<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! route('roles.index') !!}"><i class="fa fa-group"></i><span>Roles</span></a>
</li>
@endif
<li class="{{ Request::is('plans*') ? 'active' : '' }}">
    <a href="{!! route('plans.index') !!}"><i class="fa fa-money"></i><span>Plans</span></a>
</li>

