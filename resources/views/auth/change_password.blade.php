@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Change Password
    </h1>
</section>
<div class="content">
    @include('adminlte-templates::common.errors')
    <div class="box box-primary">

        <div class="box-body">
            <div class="row">
                {!! Form::open(['route' => 'changePassword']) !!}

                <div class="form-group col-sm-6">
                    {!! Form::label('oldPassword', 'Old Password:') !!}
                    {!! Form::password('oldPassword', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6" style="height: 60px;"></div>

                <div class="form-group col-sm-6">
                    {!! Form::label('newPassword', 'New Password:') !!}
                    {!! Form::password('newPassword', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6" style="height: 60px;"></div>

                <div class="form-group col-sm-6">
                    {!! Form::label('reenterPassword', 'Re-enter Password:') !!}
                    {!! Form::password('reenterPassword', ['class' => 'form-control']) !!}
                </div>
                
                <div class="form-group col-sm-12">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    <!--<a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>-->
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
